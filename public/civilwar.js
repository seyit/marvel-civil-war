var dataTeams = [
      [//Team Iron
      {axis:"Intelligence",value:520},
      {axis:"Strength",value:387},
      {axis:"Speed",value:385},
      {axis:"Durability",value:441},
      {axis:"Power",value:429},
      {axis:"Combat",value:499},
    ],[//Team Cap
      {axis:"Intelligence",value:528},
      {axis:"Strength",value:321},
      {axis:"Speed",value:363},
      {axis:"Durability",value:378},
      {axis:"Power",value:399},
      {axis:"Combat",value:540},
      ]
    ];

var IronMan = [
          [//Iron Man
          {axis:"Intelligence",value:98},
          {axis:"Strength",value:79},
          {axis:"Speed",value:73},
          {axis:"Durability",value:80},
          {axis:"Power",value:90},
          {axis:"Combat",value:71},
        ]
      ];

var WarMachine = [
          [//War Machine
          {axis:"Intelligence",value:62},
          {axis:"Strength",value:74},
          {axis:"Speed",value:67},
          {axis:"Durability",value:84},
          {axis:"Power",value:80},
          {axis:"Combat",value:77},
        ]
      ];
var BlackPanther = [
          [//Black Panther
          {axis:"Intelligence",value:90},
          {axis:"Strength",value:40},
          {axis:"Speed",value:47},
          {axis:"Durability",value:65},
          {axis:"Power",value:51},
          {axis:"Combat",value:95},
        ]
      ];

var BlackWidow = [
          [//Black Widow
          {axis:"Intelligence",value:85},
          {axis:"Strength",value:51},
          {axis:"Speed",value:59},
          {axis:"Durability",value:53},
          {axis:"Power",value:55},
          {axis:"Combat",value:97},
        ]
      ];

var TheVision = [
          [//The Vision
          {axis:"Intelligence",value:97},
          {axis:"Strength",value:77},
          {axis:"Speed",value:69},
          {axis:"Durability",value:90},
          {axis:"Power",value:87},
          {axis:"Combat",value:74},
        ]
      ];

var SpiderMan = [
          [//Spider-Man
          {axis:"Intelligence",value:88},
          {axis:"Strength",value:66},
          {axis:"Speed",value:70},
          {axis:"Durability",value:69},
          {axis:"Power",value:66},
          {axis:"Combat",value:85},
        ]
      ];

var CaptainAmerica = [
          [//Captain America
          {axis:"Intelligence",value:74},
          {axis:"Strength",value:53},
          {axis:"Speed",value:53},
          {axis:"Durability",value:65},
          {axis:"Power",value:55},
          {axis:"Combat",value:94},
        ]
      ];

var SharonCarter = [
          [//Sharon Carter
          {axis:"Intelligence",value:79},
          {axis:"Strength",value:58},
          {axis:"Speed",value:63},
          {axis:"Durability",value:64},
          {axis:"Power",value:64},
          {axis:"Combat",value:78},
        ]
      ];
var BuckyBarnes = [
          [//Bucky Barnes
          {axis:"Intelligence",value:62},
          {axis:"Strength",value:46},
          {axis:"Speed",value:41},
          {axis:"Durability",value:64},
          {axis:"Power",value:59},
          {axis:"Combat",value:87},
        ]
      ];

var Falcon = [
          [//Falcon
          {axis:"Intelligence",value:65},
          {axis:"Strength",value:31},
          {axis:"Speed",value:65},
          {axis:"Durability",value:39},
          {axis:"Power",value:37},
          {axis:"Combat",value:70},
        ]
      ];

var AntMan = [
          [//Ant-Man
          {axis:"Intelligence",value:96},
          {axis:"Strength",value:48},
          {axis:"Speed",value:38},
          {axis:"Durability",value:40},
          {axis:"Power",value:51},
          {axis:"Combat",value:52},
        ]
      ];

var Hawkeye = [
          [//Hawkeye
          {axis:"Intelligence",value:63},
          {axis:"Strength",value:28},
          {axis:"Speed",value:38},
          {axis:"Durability",value:33},
          {axis:"Power",value:37},
          {axis:"Combat",value:82},
        ]
      ];
var ScarletWitch = [
          [//Scarlet Witch
          {axis:"Intelligence",value:89},
          {axis:"Strength",value:57},
          {axis:"Speed",value:65},
          {axis:"Durability",value:73},
          {axis:"Power",value:96},
          {axis:"Combat",value:77},
        ]
      ];
